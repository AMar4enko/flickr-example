import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Actions } from 'react-native-router-flux';
import UserPhoto from './user-photo';

export default class DrawerContent extends Component {
  static get contextTypes() {
    return {
      drawer: React.PropTypes.object
    };
  }

  render() {
    return (
      <View>
        <UserPhoto />
        <TouchableOpacity
          style={styles.drawerItemContainer}
          onPress={() => {
            this.props.drawer.closeDrawer();
            Actions.search();
          }}
        >
          <Icon
            style={styles.icon}
            name='search'
            size={24}
          />
          <Text
            style={styles.text}
          >
            {'Search'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.drawerItemContainer}
          onPress={() => {
            this.props.drawer.closeDrawer();
            Actions.counter();
          }}
        >
          <Icon
            style={styles.icon}
            name='plus'
            size={24}
          />
          <Text
            style={styles.text}
          >
            {'Counter'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.drawerItemContainer}
          onPress={() => {
            this.props.drawer.closeDrawer();
            Actions.categories();
          }}
        >
          <Icon
            style={styles.icon}
            name='spinner-3'
            size={24}
          />
          <Text
            style={styles.text}
          >
            {'Categories'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  drawerItemContainer: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  text: {
    fontSize: 13,
    marginLeft: 20
  }
});