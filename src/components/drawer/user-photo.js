import React, { Component } from 'react';
import { View, Image } from 'react-native';

export default class UserPhoto extends Component {

  render() {
    return (
      <Image
        style={styles.backgroundImage}
        source={require('./assets//bg.png')}
        resizeMode='cover'
      >
        <Image
          style={styles.image}
          source={{uri: 'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-1/p160x160/12313673_1107488695928417_6795742231666953416_n.jpg?oh=7d8b8605bbdcd89b7193db29e31c66b9&oe=58068975'}}
        />
      </Image>
    );
  }
}

//https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-1/p160x160/12313673_1107488695928417_6795742231666953416_n.jpg?oh=7d8b8605bbdcd89b7193db29e31c66b9&oe=58068975

const styles = {
  backgroundImage: {
    flex: 1,
    height: 180,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  image: {
    borderRadius: 60,
    height: 120,
    width: 120,
    marginTop: 25,
    marginLeft: 10,
    borderWidth: 2,
    borderColor: '#D1C4E9'
  }
}