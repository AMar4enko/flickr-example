import React, { Component } from 'react';
import { DrawerLayoutAndroid } from 'react-native';
import DrawerContent from './content';
import { DefaultRenderer, Actions } from 'react-native-router-flux';

export default class DrawerLayout extends Component {

  render() {
    const state = this.props.navigationState;
    const children = state.children;

    return (
      <DrawerLayoutAndroid
        ref='drawer'
        drawerWidth={300}
        renderNavigationView={() => <DrawerContent drawer={this.refs.drawer}/>}
        open={state.open}
        onDrawerOpen={()=>Actions.refresh({key:state.key, open: true})}
        onDrawerClose={()=>Actions.refresh({key:state.key, open: false})}
        statusBarBackgroundColor='#512DA8'
        drawerBackgroundColor='#EDE7F6'
      >
        <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate}/>
      </DrawerLayoutAndroid>
    );
  }
}