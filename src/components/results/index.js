import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

export default class Results extends Component {

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>
          The current count is {this.props.count}
        </Text>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    count: state.counter.count
  };
}

export default connect(mapStateToProps)(Results);