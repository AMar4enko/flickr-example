import React, { Component } from 'react';
import { ScrollView, View, TextInput, StyleSheet, TouchableNativeFeedback, Text, Image } from 'react-native';
import { connect } from 'react-redux';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchString: ''
    };
  }

  onGo() {
    const searchString = this.state.searchString;
    this.props.searchByTextInput(searchString);
    this.setState({ searchString: '' });
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Image
          source={require('./assets/flickr.png')}
          style={styles.image}
        />
        <TextInput
          style={styles.searchInput}
          value={this.state.searchString}
          onChangeText={(searchString) => this.setState({searchString})}
          placeholder='What do you want to see?'
        />
         
        <TouchableNativeFeedback
          background={TouchableNativeFeedback.SelectableBackground()}
          onPress={this.onGo.bind(this)}
        >
          <View style={styles.button}>
            <Text style={styles.buttonText}>Go</Text>
          </View>
        </TouchableNativeFeedback>

        {
          this.props.receivedPhotos.map((photo) => <Text key={photo.id} style={{marginBottom: 20}}>{photo.title}</Text>)
        }
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {

  return {
    receivedPhotos: state.searchResults ? state.searchResults.photos.photo : []
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20
  },
  searchInput: {
    marginTop: 90
  },
  button: {
    height: 36,
    marginTop: 10,
    backgroundColor: '#ff4081',
    borderColor: '#ff4081',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#ede7f6'
  },
  image: {
    marginTop: 20
  }
});

export default connect(mapStateToProps)(Search);