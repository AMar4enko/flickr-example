import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class Categories extends Component {

  render() {
    return (
      <View>
        <Text
          onPress={() => {
            Actions.categories2();
          }}
        >
          {'Categories'}
        </Text>
      </View>
    );
  }
}