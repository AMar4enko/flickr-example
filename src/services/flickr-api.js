const BASE_URL = 'https://api.flickr.com/services/rest/';
const FORMAT = '&format=json&nojsoncallback=1';
const API_KEY = '&api_key=c22399eb260ead3a888d57c9db564ca2';
const METHOD = {
  search: '?method=flickr.photos.search'
};
const PER_PAGE = '&per_page=20';

export const build_url = {
  photoSearch(searchString) {
    const TEXT = `&text="${searchString}"`;
    const url = `${BASE_URL}${METHOD.search}${FORMAT}${API_KEY}${PER_PAGE}${TEXT}`;
    return url;
  }
};