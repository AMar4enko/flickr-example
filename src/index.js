import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import configureStore from './store/configure-store';
import App from './app';

const store = configureStore();
const persistor = persistStore(store, { storage: AsyncStorage });

export default class Root extends Component {
  render() {
    return (
      <Provider store={store} persistor={persistor}>
        <App />
      </Provider>
    );
  }
}