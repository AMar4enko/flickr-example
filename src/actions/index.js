const counterActions = require('./counterActions');
const searchActions = require('./searchActions');

module.exports = {
  ...counterActions,
  ...searchActions
};