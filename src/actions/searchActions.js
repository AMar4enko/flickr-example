import { build_url } from '../services/flickr-api';

export const TYPES = {
  RECEIVE_PHOTOS_SUCCESS: 'RECEIVE_PHOTOS_SUCCESS'
};

function receivePhotos(text, photos) {
  return {
    type: TYPES.RECEIVE_PHOTOS_SUCCESS,
    text,
    photos
  };
}

export function searchByTextInput(text) {
  return function(dispatch) {
    fetch(build_url.photoSearch(text))
      .then(response => response.json())
      .then(data => dispatch(receivePhotos(text, data.photos)))
      .catch((error) => console.log(error)
    );
  };
}