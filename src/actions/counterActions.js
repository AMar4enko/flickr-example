export const TYPES = {
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
  RESET: 'RESET'
};

export function increment() {
  return {
    type: TYPES.INCREMENT
  };
}

export function decrement() {
  return {
    type: TYPES.DECREMENT
  };
}

export function reset() {
  return {
    type: TYPES.RESET
  };
}