import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Router, Scene } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/EvilIcons';
import * as actionCreators from './actions';

import Search from './components/search';
import Counter from './components/counter';
//import Results from './components/results';
import Categories from './components/categories';
import Categories2 from './components/categories/categories2';
import DrawerLayout from './components/drawer';

class App extends Component {

  static get contextTypes() {
    return {
      drawer: React.PropTypes.object
    };
  }

  renderMenuIcon() {
    return (
      <TouchableOpacity style={{justifyContent: 'flex-start', 'paddingLeft': 10}} onPress={null}>
        <Icon
          style={styles.icon}
          name='navicon'
          size={36}
          color='#fff'
        />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <Router getSceneStyle={getSceneStyle}>
        <Scene key='root'>
          <Scene key='drawer' component={DrawerLayout}>
            <Scene key='main'>
              <Scene
                key='search'
                component={Search}
                navigationBarStyle={styles.navigationBarStyle}
                titleStyle={styles.titleStyle}
                title='search'
                renderLeftButton={this.renderMenuIcon}
                searchByTextInput={this.props.actions.searchByTextInput}
                type='replace'
              />
              <Scene
                key='counter'
                component={Counter}
                navigationBarStyle={styles.navigationBarStyle}
                titleStyle={styles.titleStyle}
                title='counter'
                renderLeftButton={this.renderMenuIcon}
                increment={this.props.actions.increment}
                decrement={this.props.actions.decrement}
                reset={this.props.actions.reset}
                type='replace'
              />
              <Scene
                key='categories'
                component={Categories}
                navigationBarStyle={styles.navigationBarStyle}
                titleStyle={styles.titleStyle}
                title='categories'
                renderLeftButton={this.renderMenuIcon}
                type='replace'
              />
              <Scene
                key='categories2'
                component={Categories2}
                navigationBarStyle={styles.navigationBarStyle}
                titleStyle={styles.titleStyle}
                title='categories'
                renderLeftButton={this.renderMenuIcon}
              />
            </Scene>
          </Scene>
        </Scene>
      </Router>
    );
  }
}

const getSceneStyle = function (props, computedProps) {
  const style = {
    flex: 1,
    backgroundColor: '#FAFAFA',
    shadowColor: null,
    shadowOffset: null,
    shadowOpacity: null,
    shadowRadius: null,
  };
  if (computedProps.isActive) {
    style.marginTop = computedProps.hideNavBar ? 0 : 64;
    style.marginBottom = computedProps.hideTabBar ? 0 : 50;
  }
  return style;
};

const styles = {
  navigationBarStyle: {
    backgroundColor: '#673AB7',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleStyle: {
    color: '#FFF',
    marginTop: 0
  }
};

function mapStateToProps(state) {
  return {
    count: state.counter.count
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);