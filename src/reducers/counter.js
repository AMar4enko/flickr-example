import { TYPES } from '../actions/counterActions';

const initialState = {
  count: 0
};

export default function counter(state = initialState, action) {
  switch (action.type) {
    case TYPES.INCREMENT:
      return {
        ...state,
        count: state.count + 1
      };
    case TYPES.DECREMENT:
      return {
        ...state,
        count: state.count - 1
      };
    case TYPES.RESET:
      return {
        ...state,
        count: 0
      };
    default:
      return state;
  }
}