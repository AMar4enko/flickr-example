import { TYPES } from '../actions/searchActions';

const initialState = {
  photos: {
    page: null,
    pages: null,
    photo: []
  },
  searchText: null
};

export default function searchResults(state = initialState, action) {
  switch (action.type) {
    case TYPES.RECEIVE_PHOTOS_SUCCESS:
      state = {
        ...state
      };

      state.photos.photo = action.photos.photo;
      return state;
    default:
      return state;
  }
}