import { combineReducers } from 'redux';
import counter from './counter';
import searchResults from './search';
//import name from './name';

export default combineReducers({
  counter,
  searchResults
});