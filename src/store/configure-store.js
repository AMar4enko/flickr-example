import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { autoRehydrate } from 'redux-persist';
import reducers from '../reducers';

export default function configureStore() {

  //const store = compose(applyMiddleware(thunk), autoRehydrate())(createStore)(reducers);

  const store = createStore(
    reducers,
    compose(
      applyMiddleware(
        thunk
      )
      ,
      autoRehydrate()
    )
  );

  return store;
}